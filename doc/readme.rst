Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://revesansparole.gitlab.io/sim_metrics/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/sim_metrics/1.0.0/


.. image:: https://revesansparole.gitlab.io/sim_metrics/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/sim_metrics


.. image:: https://revesansparole.gitlab.io/sim_metrics/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/sim_metrics/


.. image:: https://badge.fury.io/py/sim_metrics.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/sim_metrics




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/sim_metrics/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/sim_metrics/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/sim_metrics/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/sim_metrics/commits/main
.. #}

Set of metrics for evaluating simulation results
