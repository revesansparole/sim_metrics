import pytest

from sim_metrics import elmwise


def test_ae():
    assert elmwise.ae(3.4, 3.4) == pytest.approx(0, abs=1e-15)
    assert elmwise.ae(3.4, 4.4) == pytest.approx(1.0, abs=1e-15)
    assert elmwise.ae(9, 11) == pytest.approx(2, abs=1e-15)


def test_mae():
    assert elmwise.mae(range(0, 11), range(1, 12)) == pytest.approx(1, abs=1e-15)
    assert elmwise.mae([0, .5, 1, 1.5, 2], [0, .5, 1, 1.5, 2]) == pytest.approx(0, abs=1e-15)
    assert elmwise.mae(range(1, 5), [1, 2, 3, 5]) == pytest.approx(0.25, abs=1e-15)


def test_mse():
    assert elmwise.mse(range(0, 11), range(1, 12)) == pytest.approx(1, abs=1e-15)
    assert elmwise.mse([0, .5, 1, 1.5, 2], [0, .5, 1, 1.5, 2]) == pytest.approx(0, abs=1e-15)
    assert elmwise.mse(range(1, 5), [1, 2, 3, 6]) == pytest.approx(1.0, abs=1e-15)


def test_rmse():
    assert elmwise.rmse(range(0, 11), range(1, 12)) == pytest.approx(1, abs=1e-15)
    assert elmwise.rmse([0, .5, 1, 1.5, 2], [0, .5, 1, 1.5, 2]) == pytest.approx(0, abs=1e-15)
    assert elmwise.rmse(range(1, 5), [1, 2, 3, 5]) == pytest.approx(0.5, abs=1e-15)


def test_rrmse():
    assert elmwise.rrmse(range(1, 12), range(0, 11)) == pytest.approx(0.2, abs=1e-15)
    assert elmwise.rrmse([0, .5, 1, 1.5, 2], [0, .5, 1, 1.5, 2]) == pytest.approx(0, abs=1e-15)
    assert elmwise.rrmse([1, 2, 3, 5], range(1, 5)) == pytest.approx(0.2, abs=1e-15)


def test_se():
    assert elmwise.se(3.4, 3.4) == pytest.approx(0, abs=1e-15)
    assert elmwise.se(3.4, 4.4) == pytest.approx(1.0, abs=1e-15)
    assert elmwise.se(9, 11) == pytest.approx(4, abs=1e-15)
